# Fibonacci Zahlen berechnen.
Fibonacci:
	MOVI  R3, 0x0		# R[3] = 0x0000 
	MOVI  R4, 0x4		# R[4] = 0x0004
	MOVI  R0, 0x0		# R[0] = 0x0000 
	BSETI R0, 0xF		# R[0] = 0x8000
	MOVI  R1, 0x0		# R[1] = 0x0000
	MOVI  R2, 0x1		# R[2] = 0x0001
	STW   R1, R0, 0x0	# MEM[R[0]] = R[1]
	STW   R2, R0, 0x2	# MEM[R[0] + 2] = R[2]

LOOP:	
	CMPE  R3, R4		# R[3] == R[4]
	BT    END
	LDW   R1, R0, 0x0	# R[1] = MEM[R0]
	LDW   R2, R0, 0x2   	# R[2] = MEM[R0 + 2]
	ADDU  R1, R2		# R[1] = R[1] + R[2]   
	STW   R1, R0, 0x4	# MEM[R[0]] = R[1]
	ADDI  R0, 0x2		# R[0] = R[0] + 2
	SUBI  R4, 0x1		# R[4] = R[4] - 1  
	BR    LOOP

END:	
	HALT
	
