public class JumpAddress {
	
	private String label;
	private int address; 
	
	public JumpAddress(String label, int address) {
		this.label = label;
		this.address = address;
	}
	
	public String getLabel() {
		return label;
	}
	
	public int getAddress() {
		return address;
	}
}
