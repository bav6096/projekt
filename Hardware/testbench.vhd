library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkgProcessor.all;

entity testbench is
    generic (
		clkPeriod : time	 := 5 ns;
		clkCycles : positive := 10000
	);
end testbench;

architecture arcTestbench of testbench is         
    signal clock : std_logic;
    signal nR    : std_logic;

    -- IO signals to / from the uROM
    signal uAddr : std_logic_vector(7 downto 0);
    signal uData : std_logic_vector(35 downto 0);

    -- IO signals to / from RAM and ROM
    signal nCSROM    : std_logic;
    signal nCSRAM    : std_logic;
    signal outRegNOE : std_logic;
    signal outRegNWE : std_logic;
    signal addrBus   : std_logic_vector(15 downto 0);
    signal dataBus   : std_logic_vector(15 downto 0);
    
    -- only bits <10:0> of the address bus are 
    -- physically and only bits <10:1> of the 
    -- address bus are logically connected to 
    -- the ROM and the RAM 
    
    -- signal addrBusCircumcised : std_logic_vector(10 downto 0);

    -- SRAM control signals
    signal nCSuROM  : std_logic;
    signal opCouROM : tyOpCoSRAM;
    signal opCoROM  : tyOpCoSRAM;
    signal opCoRAM  : tyOpCoSRAM;
begin
    stimulus: process is
        variable runtime : time := clkPeriod * clkCycles;
    begin
        nR       <= '0', '1' after 5 ns;
        nCSuROM  <= '1', '0' after 5 ns;
        opCouROM <= load, none after 5 ns;
        opCoROM  <= load, none after 5 ns;
        opCoRAM  <= load, none after 5 ns;
        
        wait for 5 ns;
        for n in 1 to clkCycles loop
            clock <= '0', '1' after clkPeriod/2;
            wait for clkPeriod;
        end loop;
        
        wait for runtime;
        opCoRAM  <= store, none after 5 ns;
        wait;
    end process stimulus;

    -- addrBusCircumcised <= addrBus(10 downto 1) & '0'; 

    uROM: SRAM 
    generic map (
        cellWd => 36,
        wordWd => 1,	
        addrWd => 8,
        fileID => "memory/uROM.dat"
    )
    port map (	
        opCoSRAM => opCouROM,
        nCS      => nCSuROM,
        nWE      => '1',
        nOE      => '0',
        addr     => uAddr,
        data     => uData
    );

    ROM : SRAM 
    generic map (	
        cellWd => 8,
        wordWd => 2,
        addrWd => 11,
        fileID => "memory/Fibonacci.dat"
    )
    port map (	
        opCoSRAM => opCoROM,
        nCS      => nCSROM,
        nWE      => '1',
        nOE      => outRegNOE,
        -- addr     => addrBusCircumcised,
        addr     => addrBus(10 downto 0),
        data     => dataBus
    );

    RAM : SRAM 
    generic map (	
        cellWd => 8,
        wordWd => 2,
        addrWd => 11,
        fileID => "memory/RAM.dat"
    )
    port map (	
        opCoSRAM => opCoRAM,
        nCS      => nCSRAM,
        nWE      => outRegNWE,
        nOE      => outRegNOE,
        -- addr     => addrBusCircumcised,
        addr     => addrBus(10 downto 0),
        data     => dataBus
    );

    instProcessor : Processor 
    port map ( 
        clock     => clock,
        nR        => nR,
        uAddr     => uAddr,
        uData     => uData,
        nCSROM    => nCSROM,
        nCSRAM    => nCSRAM,
        outRegNOE => outRegNOE,
        outRegNWE => outRegNWE,
        addrBus   => addrBus,
        dataBus   => dataBus
    );  
end arcTestbench;
