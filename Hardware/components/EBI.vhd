library ieee;
use ieee.std_logic_1164.all;

entity EBI is
    port ( 
        --------------------------------------------------
        -- external processor signals --------------------
        -------------------------------------------------- 
        
        clock : in std_logic;
        nR    : in std_logic;

        -- IO signals to / from the ROM and RAM
        nCSROM    : out   std_logic;
        nCSRAM    : out   std_logic;
        outRegNOE : out   std_logic;
        outRegNWE : out   std_logic;
        addrBus   : inout std_logic_vector(15 downto 0);
        dataBus   : inout std_logic_vector(15 downto 0);
        
        --------------------------------------------------
        -- internal processor signals --------------------
        --------------------------------------------------  
        
        omnibus : inout std_logic_vector(15 downto 0);
        
        -- signals that are required by the nOE 
        -- and nWE registers
        nOE : in std_logic;
        nWE : in std_logic;
        
        -- signals that are required by the MAR,
        -- MDR and MRR registers
        eMAR : in std_logic;
        eMDR : in std_logic;
        eMRR : in std_logic;
        
        -- signals that are required by buffers
        eBuffMDR : in std_logic;
        eBuffMRR : in std_logic
    );
end EBI;

architecture arcEBI of EBI is
    -- output signal of the MDR register: 
    -- from MDR register to MDR buffer         
    signal outMDR : std_logic_vector(15 downto 0);

    -- output signal of the MRR register: 
    -- from MRR register to MRR buffer  
    signal outMRR : std_logic_vector(15 downto 0);
begin    
    regNOE : process (clock) is 
    begin
        if rising_edge(clock) then
            outRegNOE <= nOE;        
        end if;    
    end process regNOE;

    regNWE : process (clock) is 
    begin
        if rising_edge(clock) then
            outRegNWE <= nWE;        
        end if;    
    end process regNWE;

    -- MSB stands for most significant bit
    MSB : process (addrBus) is
    begin
        if addrBus(15) = '0' then
            nCSROM <= '0';
            nCSRAM <= '1';
        else 
            nCSROM <= '1';
            nCSRAM <= '0';
        end if;
    end process MSB;

    MAR : process (clock, nR, eMAR) is
    begin
        if nR = '0' then
            addrBus <= (others => '0');
        else
            if rising_edge(clock) then
                if eMAR = '1' then
                    addrBus <= omnibus;
                end if;
            end if;
        end if;
    end process MAR;

    MDR : process (clock, nR, eMDR) is
    begin
        if nR = '0' then
            outMDR <= (others => '0');
        else 
            if rising_edge(clock) then
                if eMDR = '1' then
                    outMDR <= omnibus;
                end if;
            end if;
        end if;
    end process MDR;

    buffMDR : process (outMDR, eBuffMDR) is
    begin 
        if eBuffMDR = '1' then
            dataBus <= outMDR;
        else 
            dataBus <= (others => 'Z');
        end if;
    end process buffMDR;

    MRR : process (clock, nR, eMRR) is
    begin
        if nR = '0' then
            outMRR <= (others => '0');
        else 
            if rising_edge(clock) then
                if eMRR = '1' then
                    outMRR <= dataBus;
                end if;
            end if;
        end if;
    end process MRR;

    buffMRR : process (outMRR, eBuffMRR) is
    begin 
        if eBuffMRR = '1' then
            omnibus <= outMRR;
        else 
            omnibus <= (others => 'Z');
        end if;
    end process buffMRR;
end arcEBI;
