library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
	port ( 	
		--------------------------------------------------
        -- external processor signals --------------------
        -------------------------------------------------- 
		
		clock : in std_logic;
		nR    : in std_logic;
		
		--------------------------------------------------
        -- internal processor signals --------------------
        --------------------------------------------------  

		omnibus : out std_logic_vector(15 downto 0);

		-- signals that are required by the adder
		imm12 : in std_logic_vector(11 downto 0);

		-- signals that are required by the 4 to 1 
		-- multiplexer
		muxPC : in std_logic_vector(1 downto 0); 	
		DX    : in std_logic_vector(15 downto 0); 	
		
		-- signals that are required by the PC 
		-- register and PC buffer
		ePC     : in std_logic;
		eBuffPC : in std_logic
	);
end PC;

architecture arcPC of PC is
	-- output signal of the PC register: 
    -- from PC register to adder and PC buffer   
	signal outPC : std_logic_vector(15 downto 0);
	
	-- output signal of adder
    -- from adder to 4 to 1 multiplexer   
	signal outAdd : std_logic_vector(15 downto 0);

	-- output signal of the 4 to 1 multiplexer
    -- from 4 to 1 multiplexer to PC register  
	signal outMux : std_logic_vector(15 downto 0);
begin
	adder : process (imm12, outPC) is
	begin
		outAdd <= std_logic_vector(signed(outPC) + resize(signed(imm12), outPC'length));
	end process adder;
	
	mux4: process (muxPC, DX, outAdd, outPC) is 
	begin
		case muxPC is
			when "00"   => outMux <= std_logic_vector(unsigned(outPC) + 2);
			when "01"   => outMux <= outAdd;
			when "10"   => outMux <= DX;
			when others => outMux <= (others => '0');  
		end case;
	end process mux4;

	regPC: process (clock, nR, ePC) is
	begin 
		if nR = '0' then
			outPC <= (others => '0');
		else 
			if rising_edge(clock) then
				if ePC = '1' then 
					outPC <= outMux;
				end if;
			end if;
		end if;
	end process regPC;

	buffPC: process (outPC, eBuffPC) is
	begin 
		if eBuffPC = '1'then 
			omnibus <= outPC;
		else 
			omnibus <= (others => 'Z');
		end if;
	end process buffPC;
end arcPC;