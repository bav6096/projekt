library ieee;
use ieee.std_logic_1164.all;

entity CU is
    port (
        --------------------------------------------------
        -- external processor signals --------------------
        -------------------------------------------------- 
        
        clock   : in std_logic;
        nR      : in std_logic;
        
        -- IO signals to / from the uROM
        uData : in  std_logic_vector(35 downto 0);
        uAddr : out std_logic_vector(7 downto 0);           
        
        --------------------------------------------------
        -- internal processor signals --------------------
        --------------------------------------------------   
        
        omnibus : in std_logic_vector(15 downto 0);
        
        -- signal that are required by the control unit
        carry   : in std_logic;

        -- signals that are required for memory addressing
        offset4  : out std_logic_vector(3 downto 0);
        eBuffAdd : out std_logic;
        
        -- signals that are required to increase the program 
        -- counter
        imm12   : out std_logic_vector(11 downto 0);
        muxPC   : out std_logic_vector(1 downto 0);
        ePC     : out std_logic;
        eBuffPC : out std_logic;
        
        -- signals that are required to control the external 
        -- bus interface
        nWE      : out std_logic;
        nOE      : out std_logic;
        eMAR     : out std_logic;
        eMDR     : out std_logic;
        eMRR     : out std_logic;
        eBuffMDR : out std_logic;
        eBuffMRR : out std_logic;

        -- signals that are required by the datapath (ALU,
        -- register file and C register)
        opCoALU  : out std_logic_vector(4 downto 0);
        imm4     : out std_logic_vector(3 downto 0);
        AX       : out std_logic_vector(3 downto 0);
        AY       : out std_logic_vector(3 downto 0);
        eC       : out std_logic;
        eAX15    : out std_logic;
        regNWE   : out std_logic;
        eBuffDX  : out std_logic;
        eBuffALU : out std_logic
    );
end CU;

architecture arcCU of CU is         
    -- output signal of the instruction register: 
    -- from instruction register to decoder
    signal I : std_logic_vector(15 downto 0);

    -- output signal of the decoder: 
    -- from decoder to uPCmux   
    signal uI : std_logic_vector(7 downto 0);

    -- output singal of the 2 to 1 mux: 
    -- from 2 to 1 mux to 4 to 1 mux
    signal outMux2 : std_logic_vector(7 downto 0);

    -- output signal of the 4 to 1 mux: 
    -- from 4 to 1 mux to uPC
    signal outMux4 : std_logic_vector(7 downto 0);

    -- output signals of the uROM used to determine the 
    -- next stat of the sequential circuit
    signal nextA  : std_logic_vector(7 downto 0); 
    signal nextB  : std_logic_vector(7 downto 0);
    signal muxuPC : std_logic_vector(1 downto 0);
    signal eIR    : std_logic;
begin
    IR : process (eIR, clock, nR) is 
    begin
        if nR = '0' then 
            I <= (others => '0');
        else
            if rising_edge(clock) then
                if eIR = '1' then
                    I <= omnibus;
                end if; 
            end if;       
        end if;    
    end process IR;

    decoder : process (I) is 
    begin
        uI      <= I(15 downto 12) & "0000"; 
        imm12   <= I(11 downto 0);
        -- the twelfth bit is used twice, once for the OPC and a 
        -- second time for the sub-OPC or ALU-OPC
        opCoALU <= I(12 downto 8);
        offset4 <= I(11 downto 8);
        imm4    <= I(7 downto 4);
        AY      <= I(7 downto 4);
        AX      <= I(3 downto 0);
    end process decoder;
    
    mux2 : process (carry, nextB, nextA) is 
    begin    
        case carry is
            when '0'    => outMux2 <= nextA;
            when others => outMux2 <= nextB;
        end case;
    end process mux2;
    
    mux4 : process (muxuPC, uI, outMux2, nextB, nextA) is 
    begin
        case muxuPC is
            when "00"   => outMux4 <= nextA;
            when "01"   => outMux4 <= nextB;
            when "10"   => outMux4 <= outMux2;
            when others => outMux4 <= uI;
        end case;
    end process mux4;

    uPC : process (clock, nR) is 
    begin
        if nR = '0' then 
            uAddr <= (others => '0');
        else 
            if rising_edge(clock) then
                uAddr <= outMux4; 
            end if;       
        end if;    
    end process uPC;

    signalSplitter : process (uData) is
    begin
        nextA    <= uData(35 downto 28);
        nextB    <= uData(27 downto 20);
        muxuPC   <= uData(19 downto 18);
        eBuffDX  <= uData(17);
        eAX15    <= uData(16);
        eIR      <= uData(15);
        eBuffAdd <= uData(14);
        eC       <= uData(13);
        eBuffALU <= uData(12);
        regNWE   <= uData(11);
        eBuffPC  <= uData(10);
        ePC      <= uData(9);
        muxPC    <= uData(8 downto 7);
        eBuffMRR <= uData(6);
        eMRR     <= uData(5);
        eMDR     <= uData(4);
        eBuffMDR <= uData(3);
        eMAR     <= uData(2);
        nWE      <= uData(1);
        nOE      <= uData(0); 
    end process signalSplitter;
end arcCU;
