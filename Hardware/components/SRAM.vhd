library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

use std.textio.all;
use work.pkgProcessor.all;

entity SRAM is
    generic (	
        -- The memory cell width in bits.
        cellWd : integer range 1 to 64;
        -- The number of cells a memory word is 
        -- composed of.
        wordWd : integer range 1 to 16;
        -- The memory address width in bits.
        addrWd : integer range 1 to 64;
        -- The name of the file which represents 
        -- the content of the memory. 	        	        
        fileId : string                    
    ); 
    port (		
        opCoSRAM : in    tyOpCoSRAM;
        nCS      : in    std_logic; 
        nWE	     : in    std_logic;  
        nOE	     : in    std_logic;  
        addr     : in    std_logic_vector(addrWd-1 downto 0);
        data     : inout std_logic_vector(cellWd*wordWd-1 downto 0)
    );
end entity SRAM;

architecture arcSRAM of SRAM is
begin
    procSRAM: process (nCS, nWE, nOE, addr, data, opCoSRAM) is
        constant addrSpace : natural := (2**addrWd)-1;
        constant dataWd    : natural := cellWd*wordWd;

        subtype eleTySRAM is std_logic_vector(dataWd-1 downto 0);
        type    memTySRAM is array (0 to addrSpace) of eleTySRAM;

        variable memSRAM : memTySRAM;		

        file	 fileIO : text;			
        variable statIO : file_open_status;	
        variable lineIO : line;			
        variable rdStat : boolean;		
        variable addrIO : std_logic_vector(addrWd-1 downto 0);
        variable dataIO : std_logic_vector(dataWd-1 downto 0);

        variable i : integer := 0;
    begin
        if opCoSRAM'event then
            if opCoSRAM = store then	
                file_open(statIO, fileIO, fileId, write_mode);
	            assert statIO = open_ok
	                report "SRAM - store: error opening data file"
	                severity error;
                while i <= addrSpace loop
                    write(lineIO, std_logic_vector(to_unsigned(i, addrWd)));
                    write(lineIO, ' ');	
                    write(lineIO, std_logic_vector(memSRAM(i)));
                    writeline(fileIO, lineIO); 
                    i := i + wordWd;
                end loop;
	            file_close(fileIO);
            elsif opCoSRAM = load then	
	            file_open(statIO, fileIO, fileId, read_mode);
	            assert statIO = open_ok
	                report "SRAM - load: error opening data file"
	                severity error;
	            while not endfile(fileIO) loop
                    readline(fileIO, lineIO);			
                    read(lineIO, addrIO, rdStat);
                    if rdStat then
                        read(lineIO, dataIO, rdStat);
                    end if;
                    if rdStat then
                        memSRAM(to_integer(unsigned(addrIO))) := dataIO;
	                else
	                    report "SRAM - load: format error in data file"
	                    severity error;
	                end if;
	            end loop;
	            file_close(fileIO);
            end if;	
        end if;	

        if nCS'event then assert not Is_X(nCS)
	        report "SRAM: nCS - X value"
		    severity warning;
        end if;

        if nWE'event then assert not Is_X(nWE)
		    report "SRAM: nWE - X value"
		    severity warning;
        end if;

        if nOE'event then assert not Is_X(nOE)
		    report "SRAM: nOE - X value"
		    severity warning;
        end if;

        if addr'event then assert not Is_X(addr)
		    report "SRAM: addr - X value"
		    severity warning;
        end if;

        data <= (others => 'Z');

        if nCS = '0' then				
            if nWE = '0' then				
                memSRAM(to_integer(unsigned(addr))) := data;
            elsif nWE = '1' and nOE ='0' then	     
                data <= memSRAM(to_integer(unsigned(addr)));
            end if;
        end if;
    end process procSRAM;
end architecture arcSRAM;
