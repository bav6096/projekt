library ieee;
use ieee.std_logic_1164.all;

package pkgProcessor is   
    type tyOpCoSRAM is (none, store, load);

    component SRAM is
        generic (	
            cellWd : integer range 1 to 64;
            wordWd : integer range 1 to 16;
            addrWd : integer range 1 to 64; 	        	        
            fileId : string                    
        ); 
        port (		
            opCoSRAM : in    tyOpCoSRAM;
            nCS      : in    std_logic; 
            nWE	     : in    std_logic;  
            nOE	     : in    std_logic;  
            addr     : in    std_logic_vector(addrWd-1 downto 0);
            data     : inout std_logic_vector(cellWd*wordWd-1 downto 0)
        );
    end component SRAM;
    
    component CU is
        port (
            clock    : in  std_logic;
            nR       : in  std_logic;
            uData    : in  std_logic_vector(35 downto 0);
            uAddr    : out std_logic_vector(7 downto 0);           
            omnibus  : in  std_logic_vector(15 downto 0);
            carry    : in  std_logic;
            offset4  : out std_logic_vector(3 downto 0);
            eBuffAdd : out std_logic;
            imm12    : out std_logic_vector(11 downto 0);
            muxPC    : out std_logic_vector(1 downto 0);
            ePC      : out std_logic;
            eBuffPC  : out std_logic;
            nWE      : out std_logic;
            nOE      : out std_logic;
            eMAR     : out std_logic;
            eMDR     : out std_logic;
            eMRR     : out std_logic;
            eBuffMDR : out std_logic;
            eBuffMRR : out std_logic;
            opCoALU  : out std_logic_vector(4 downto 0);
            imm4     : out std_logic_vector(3 downto 0);
            AX       : out std_logic_vector(3 downto 0);
            AY       : out std_logic_vector(3 downto 0);
            eC       : out std_logic;
            eAX15    : out std_logic;
            regNWE   : out std_logic;
            eBuffDX  : out std_logic;
            eBuffALU : out std_logic
        );
    end component CU;

    component EBI is
        port ( 
            clock     : in    std_logic;
            nR        : in    std_logic;
            nCSROM    : out   std_logic;
            nCSRAM    : out   std_logic;
            outRegNOE : out   std_logic;
            outRegNWE : out   std_logic;
            addrBus   : inout std_logic_vector(15 downto 0);
            dataBus   : inout std_logic_vector(15 downto 0); 
            omnibus   : inout std_logic_vector(15 downto 0);
            nOE       : in    std_logic;
            nWE       : in    std_logic;
            eMAR      : in    std_logic;
            eMDR      : in    std_logic;
            eMRR      : in    std_logic;
            eBuffMDR  : in    std_logic;
            eBuffMRR  : in    std_logic
        );
    end component EBI;

    component PC is
        port ( 	
            clock   : in  std_logic;
            nR      : in  std_logic;
            omnibus : out std_logic_vector(15 downto 0);
            imm12   : in  std_logic_vector(11 downto 0);
            muxPC   : in  std_logic_vector(1 downto 0); 	
            DX      : in  std_logic_vector(15 downto 0); 	
            ePC     : in  std_logic;
            eBuffPC : in  std_logic
        );
    end component PC;

    component datapath is
        port (   
            clock    : in    std_logic;
            nR       : in    std_logic;
            omnibus  : inout std_logic_vector(15 downto 0);
            AX       : in    std_logic_vector(3 downto 0);
            AY       : in    std_logic_vector(3 downto 0);
            eAX15    : in    std_logic;
            regNWE   : in    std_logic;
            opCoALU  : in    std_logic_vector(4 downto 0);
            carry    : inout std_logic;
            imm4     : in    std_logic_vector(3 downto 0);
            DX       : inout std_logic_vector(15 downto 0);
            DY       : inout std_logic_vector(15 downto 0);
            eC       : in    std_logic;
            eBuffDX  : in    std_logic;
            eBuffALU : in    std_logic
        );
    end component datapath;

    component processor is
        port (
            clock     : in    std_logic;
            nR        : in    std_logic;
            uAddr     : out   std_logic_vector(7 downto 0);
            uData     : in    std_logic_vector(35 downto 0);
            nCSROM    : out   std_logic;
            nCSRAM    : out   std_logic;
            outRegNOE : out   std_logic;
            outRegNWE : out   std_logic;
            addrBus   : inout std_logic_vector(15 downto 0);
            dataBus   : inout std_logic_vector(15 downto 0)
        );
    end component processor;
end package pkgProcessor;