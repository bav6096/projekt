library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkgProcessor.all;

entity processor is
    port (
        --------------------------------------------------
        -- external processor signals --------------------
        -------------------------------------------------- 
        
        clock : in std_logic;
        nR    : in std_logic;

        -- IO signals to / from the uROM
        uAddr : out std_logic_vector(7 downto 0);
        uData : in  std_logic_vector(35 downto 0);

        -- IO signals to / from RAM and ROM
        nCSROM    : out   std_logic;
        nCSRAM    : out   std_logic;
        outRegNOE : out   std_logic;
        outRegNWE : out   std_logic;
        addrBus   : inout std_logic_vector(15 downto 0);
        dataBus   : inout std_logic_vector(15 downto 0)
    );
end processor;

architecture arcProcessor of processor is         
    signal omnibus : std_logic_vector(15 downto 0);

    -- signal that are required by the control unit
    signal carry : std_logic;

    -- signals that are required for memory addressing
    signal DY       : std_logic_vector(15 downto 0);
    signal offset4  : std_logic_vector(3 downto 0);
    signal outAdd   : std_logic_vector(15 downto 0);
    signal eBuffAdd : std_logic;

    -- signals that are required to increase the program 
    -- counter
    signal imm12   : std_logic_vector(11 downto 0);
    signal DX      : std_logic_vector(15 downto 0);
    signal muxPC   : std_logic_vector(1 downto 0);
    signal ePC     : std_logic;
    signal eBuffPC : std_logic;

    -- signals that are required to control the external 
    -- bus interface
    signal nWE      : std_logic;
    signal nOE      : std_logic;
    signal eMAR     : std_logic;
    signal eMDR     : std_logic;
    signal eMRR     : std_logic;
    signal eBuffMDR : std_logic;
    signal eBuffMRR : std_logic;
    
    -- signals that are required by the datapath (ALU,
    -- register file and C register)
    signal opCoAlU  : std_logic_vector(4 downto 0);
    signal imm4     : std_logic_vector(3 downto 0);
    signal AX       : std_logic_vector(3 downto 0);
    signal AY       : std_logic_vector(3 downto 0);
    signal eC       : std_logic;
    signal eAX15    : std_logic;
    signal regNWE   : std_logic;
    signal eBuffDX  : std_logic;
    signal eBuffALU : std_logic;
begin
    instCU : CU 
    port map (	
        clock    => clock,
        nR       => nR,
        uData    => uData,
        uAddr    => uAddr,           
        omnibus  => omnibus,
        carry    => carry,
        offset4  => offset4,
        eBuffAdd => eBuffAdd,
        imm12    => imm12,
        muxPC    => muxPC,
        ePC      => ePC,
        eBuffPC  => eBuffPC,
        nWE      => nWE,
        nOE      => nOE,
        eMAR     => eMAR,
        eMDR     => eMDR,
        eMRR     => eMRR,
        eBuffMDR => eBuffMDR,
        eBuffMRR => eBuffMRR,
        opCoALU  => opCoALU,
        imm4     => imm4,
        AX       => AX,
        AY       => AY,
        eC       => eC,
        eAX15    => eAX15,
        regNWE   => regNWE,
        eBuffDX  => eBuffDX,
        eBuffALU => eBuffALU
    ); 

    instEBI : EBI 
    port map ( 
        clock     => clock,
        nR        => nR,
        nCSROM    => nCSROM,
        nCSRAM    => nCSRAM,
        outRegNOE => outRegNOE,
        outRegNWE => outRegNWE,
        addrBus   => addrBus,
        dataBus   => dataBus, 
        omnibus   => omnibus,
        nOE       => nOE,
        nWE       => nWE,
        eMAR      => eMAR,
        eMDR      => eMDR,
        eMRR      => eMRR,
        eBuffMDR  => eBuffMDR,
        eBuffMRR  => eBuffMRR
    );

    instPC : PC 
    port map ( 
        clock   => clock,
        nR      => nR,
        omnibus => omnibus,
        imm12   => imm12,
        muxPC   => muxPC, 	
        DX      => DX, 	
        ePC     => ePC,
        eBuffPC => eBuffPC
    );

    instDatapath : datapath 
    port map ( 
        clock    => clock,
        nR       => nR,
        omnibus  => omnibus,
        AX       => AX,
        AY       => AY,
        eAX15    => eAX15,
        regNWE   => regNWE,
        opCoALU  => opCoALU,
        carry    => carry,
        imm4     => imm4,
        DX       => DX,
        DY       => DY,
        eC       => eC,
        eBuffDX  => eBuffDX,
        eBuffALU => eBuffALU
    );
 
    adder : process (offset4, DY) is
    begin     
        outAdd <= std_logic_vector(unsigned(DY)
                                   + resize(shift_left(unsigned(offset4), 1), outAdd'length));
    end process adder;

    buffAdd : process (outAdd, eBuffAdd) is
    begin
        if eBuffAdd = '1' then
            omnibus <= outAdd;
        else 
            omniBus <= (others => 'Z');
        end if;
    end process buffAdd;
end arcProcessor;
